﻿using System.IO;
using System;

namespace Berseneva.Lab4.Exercise1
{
    public class OperationWithOverloading
    {
        public static int add(int a, int b)
        {
            int аResult = (5 * a) + 3;
            int bResult = (-2 * b) + 13 / 3;
            return Math.Max(аResult, bResult);
        }
        public static double add(char a, char b)
        {
            double numericA = char.GetNumericValue(a);
            double numericB = char.GetNumericValue(b);
            double аResult = (5 * numericA) + 3;
            double bResult = (-2 * numericB) + 13 / 3;
            return Math.Max(аResult, bResult);
        }
        public static double add(double a, double b)
        {
            double аResult = (5 * a) + 3;
            double bResult = (-2 * b) + 13 / 3;
            return Math.Max(аResult, bResult);
        }

    }
    public class Results
    {
        public static void Main()
        {
            var testWithNumbersResult = OperationWithOverloading.add(19, 20);
            Console.WriteLine("testWithNumbersResult  - {0}", testWithNumbersResult);

            var testWithDoublesResult = OperationWithOverloading.add(19.5, 20.5);
            Console.WriteLine("testWithDoublesResult  - {0}", testWithDoublesResult);

            var testWithCharsResult = OperationWithOverloading.add('1', '2');
            Console.WriteLine("testWithCharsResult  - {0}", testWithCharsResult);
        }
    }
}

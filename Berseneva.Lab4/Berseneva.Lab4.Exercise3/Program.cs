﻿using System;

namespace Berseneva.Lab4.Exercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            double k;
            k = 1;
            Cut(k);
            static double Cut(double k)
            {
                double result = (k + 1) / 3.0;
                Console.WriteLine("f(n)=" + result);
                return result;
            }
            static double MyFunctionRecursionSum(double k) => (k == 1) ? Cut(1) : Cut(k) + MyFunctionRecursionSum(k - 1);
            static double MyFunctionRecursionSum2(double k) => (k == 0) ? 1 : (k + 1) / 3.0 * MyFunctionRecursionSum2(k - 1);
            Console.ReadLine();
        }
    }
}

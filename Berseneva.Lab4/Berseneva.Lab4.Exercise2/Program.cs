﻿using System;

namespace Berseneva.Lab4.Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            double k;
            k = 1;
            Hut(k);
            static double Hut(double k)
            {
                double result = (k + 1) / 3.0;
                Console.WriteLine("f(n)=" + result);
                return result;
            }
            static double MyFunctionRecursionSum(double k) => (k == 1) ? Hut(1) : Hut(k) + MyFunctionRecursionSum(k - 1);
            static double MyFunctionRecursionSum2(double k) => (k == 0) ? 1 : (k + 1) / 3.0 * MyFunctionRecursionSum2(k - 1);
            Console.ReadLine();

                
        }
    }
}

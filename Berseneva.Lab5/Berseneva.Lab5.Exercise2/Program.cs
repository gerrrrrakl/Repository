﻿using System;

namespace Berseneva.Lab5.Exercise2
{
    class Program
    {
        static void Main()
        {
            double sum = 0;
            double prod = 1;
            Console.Write("Введите x=");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите n=");
            int n = Convert.ToInt32(Console.ReadLine());
            for (int k = 1; k <= n; k++)
            {
                sum += 10 * x / Math.Pow(Math.E, k);
                prod *= 10 * x / Math.Pow(Math.E, k);
            }
            Console.WriteLine($"Сумма ряда равна {sum}");
            Console.WriteLine($"Произведение ряда равно {prod}");
        }
    }
}

﻿using System;

namespace Berseneva.Lab5.Exercise1
{
    class Program
    {
        //Цикл For:
        /*static void Main()
        {
        double func = 0;
        for (double k = 0;k <= 1;k += 0.1)
        {
        for (int x = 0; x <=10; x++)
        {
        func = 10 * x / Math.Pow(Math.E,k);
        Console.Write($" {func}");
        }
        Console.WriteLine();
        }
        }*/
        
        //Цикл While
        /*static void Main()
        {
        double func = 0;
        double k = 0;
        while(k<=1)
        {
        int x = 0;
        while ( x <= 10)
        {
        func = 10 * x / Math.Pow(Math.E, k);
        Console.Write($" {func}");
        x++;
        }
        k += 0.1;
        Console.WriteLine();
        }
        }*/
        
        //Цикл Do/While
        static void Main()
        {
            double func = 0;
            double k = 0;
            do
            {
                int x = 0;
                do
                {
                    func = 10 * x / Math.Pow(Math.E, k);
                    Console.Write($" {func}");
                    x++;
                } while (x <= 10);
                k += 0.1;
                Console.WriteLine();
            } while (k <= 1);
        }
    }
}

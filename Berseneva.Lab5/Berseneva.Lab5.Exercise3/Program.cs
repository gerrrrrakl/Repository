﻿using System;

namespace Berseneva.Lab5.Exercise3
{
    class Program
    {
        static void Main()
        {
            int summ = 0;
            int i = 0;
            string number = Convert.ToString(Console.ReadLine());

            foreach (char n in number)
            {
                int num = n - '0';
                i++;
                if (i == 1)
                {
                    summ += num;
                }
                else if (i == number.Length)
                {
                    summ += num;
                }
            }

            Console.WriteLine($"Сумма первой и последней цифр = {summ}");
        }
    }
}

